import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'

function Mapa(props) {

    const position = props.data;

    let marcadores = position.map((marca,i) =>{
        
        return(
            <Marker key={i} position={[marca[0], marca[1]]}>
                    <Popup>
                       {marca[2]} <br /> Bicis Disponibles: {marca[3]}
                       <br/> Slots libres: {marca[4]}
                    </Popup>
                </Marker>
        );
    })

    return (
        <div id='map'>
            <MapContainer center={[41.390844, 2.155980]} zoom={13}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {marcadores}
            </MapContainer>
        </div>
    );
}

export default Mapa;