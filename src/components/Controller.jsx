import { useEffect, useState } from "react";
import TablaDinamica from "./TablaDinamica";
import { Button, Container } from "react-bootstrap"
import Mapa from "./Mapa";

function Controller() {
    const [data, setData] = useState([])
    const [filtrar, setFiltrar] = useState("")
    // const [cargado, setCargado] = useState(false)
    const header = ["Estacion", "Bicis Disponibles", "Slots libres", "Latitud", "Longitud"]
    let posiciones = []

    async function cargarDatos() {
        fetch("https://api.citybik.es/v2/networks/bicing")
            .then(response => response.json())
            .then(result => {
                if(filtrar !== ""){
                    setData((result.network.stations).filter(bicis => bicis.free_bikes >= parseInt(filtrar)))
                }else{
                    setData(result.network.stations)
                }
                // setCargado(true)
            })
            .catch((e) => console.log("Error: " + e))
    }

    // useEffect(() =>{
    //     cargarDatos()
    // }, [filtrar])

    function fillRows(bici, i) {
        posiciones.push([bici.latitude, bici.longitude, bici.name, bici.free_bikes, bici.empty_slots])
        return (
            <tr key={i}>
                <td>{bici.name}</td>
                <td>{bici.free_bikes}</td>
                <td>{bici.empty_slots}</td>
                <td>{bici.latitude}</td>
                <td>{bici.longitude}</td>
            </tr>
        )

    }

    let rows = data.map((bici, i) => fillRows(bici, i))
    
    return (
        <div>
            <h1>BICIS</h1>
            <Container>
                <div>
                <Mapa data={posiciones} />
                </div>
            
                <div className="botonera">
                        <Button onClick={cargarDatos}>Cargar bicis</Button>
                        <input className="filtro" type="text" onInput={(e) => setFiltrar(e.target.value)} placeholder="Numero de bicis"/>
                </div>
                <TablaDinamica head={header} body={rows} />
                
            </Container>
            

        </div>
    );
}

export default Controller;