import { Container, Table } from "react-bootstrap";


function TablaDinamica(props) {

    //head -> Se le pasan los campos de la cabecera
    //body - Se le pasa ya el array de tr para que lo printe

    let cabecera = props.head.map((element,i) => {
        return <th key={i}>{element}</th>
    });

    return (

        <Table bordered hover>
            <thead>
                <tr>
                    {cabecera}
                </tr>
            </thead>
            <tbody>
                {props.body}
            </tbody>
        </Table>

    );
}

export default TablaDinamica;