import { useState } from 'react'
import logo from './logo.svg'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Controller from './components/Controller';
import 'leaflet/dist/leaflet.css'

function App() {
  return (
    <div className="App">
      <Controller />
    </div>
  )
}

export default App
